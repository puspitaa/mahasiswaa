<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Datamahasiswa extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('Datamahasiswa_model');
        $this->load->library('template');
        $this->load->helper(array('form', 'url'));
        $this->load->library('form_validation');
    }

    public function index()
    {
        $q = urldecode($this->input->get('q', TRUE));
        $start = intval($this->input->get('start'));
        
        if ($q <> '') {
            $config['base_url'] = base_url() . 'datamahasiswa/index.html?q=' . urlencode($q);
            $config['first_url'] = base_url() . 'datamahasiswa/index.html?q=' . urlencode($q);
        } else {
            $config['base_url'] = base_url() . 'datamahasiswa/index.html';
            $config['first_url'] = base_url() . 'datamahasiswa/index.html';
        }

        $config['per_page'] = 10;
        $config['page_query_string'] = TRUE;
        $config['total_rows'] = $this->Datamahasiswa_model->total_rows($q);
        $datamahasiswa = $this->Datamahasiswa_model->get_limit_data($config['per_page'], $start, $q);

        $this->load->library('pagination');
        $this->pagination->initialize($config);

        $data = array(
            'datamahasiswa_data' => $datamahasiswa,
            'q' => $q,
            'pagination' => $this->pagination->create_links(),
            'total_rows' => $config['total_rows'],
            'start' => $start,
        );
        $this->template->templatemahasiswa('datamahasiswa/datamahasiswa_list', $data);
    }

    public function read($id) 
    {
        $row = $this->Datamahasiswa_model->get_by_id($id);
        if ($row) {
            $data = array(
		'id' => $row->id,
		'nim' => $row->nim,
		'nama' => $row->nama,
		'tempatlahir' => $row->tempatlahir,
		'tanggallahir' => $row->tanggallahir,
		'alamat' => $row->alamat,
	    );
           $this->template->templatemahasiswa('datamahasiswa/datamahasiswa_read', $data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('datamahasiswa'));
        }
    }

    public function create() 
    {
        $data = array(
            'button' => 'Create',
            'action' => site_url('datamahasiswa/create_action'),
	    'id' => set_value('id'),
	    'nim' => set_value('nim'),
	    'nama' => set_value('nama'),
	    'tempatlahir' => set_value('tempatlahir'),
	    'tanggallahir' => set_value('tanggallahir'),
	    'alamat' => set_value('alamat'),
	);
        $this->template->templatemahasiswa('datamahasiswa/datamahasiswa_form', $data);
    }
    
    public function create_action() 
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->create();
        } else {
            $data = array(
		'nim' => $this->input->post('nim',TRUE),
		'nama' => $this->input->post('nama',TRUE),
		'tempatlahir' => $this->input->post('tempatlahir',TRUE),
		'tanggallahir' => $this->input->post('tanggallahir',TRUE),
		'alamat' => $this->input->post('alamat',TRUE),
	    );

            $this->Datamahasiswa_model->insert($data);
            $this->session->set_flashdata('message', 'Create Record Success');
            redirect(site_url('datamahasiswa'));
        }
    }
    
    public function update($id) 
    {
        $row = $this->Datamahasiswa_model->get_by_id($id);

        if ($row) {
            $data = array(
                'button' => 'Update',
                'action' => site_url('datamahasiswa/update_action'),
		'id' => set_value('id', $row->id),
		'nim' => set_value('nim', $row->nim),
		'nama' => set_value('nama', $row->nama),
		'tempatlahir' => set_value('tempatlahir', $row->tempatlahir),
		'tanggallahir' => set_value('tanggallahir', $row->tanggallahir),
		'alamat' => set_value('alamat', $row->alamat),
	    );
            $this->template->templatemahasiswa('datamahasiswa/datamahasiswa_form', $data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('datamahasiswa'));
        }
    }
    
    public function update_action() 
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->update($this->input->post('id', TRUE));
        } else {
            $data = array(
		'nim' => $this->input->post('nim',TRUE),
		'nama' => $this->input->post('nama',TRUE),
		'tempatlahir' => $this->input->post('tempatlahir',TRUE),
		'tanggallahir' => $this->input->post('tanggallahir',TRUE),
		'alamat' => $this->input->post('alamat',TRUE),
	    );

            $this->Datamahasiswa_model->update($this->input->post('id', TRUE), $data);
            $this->session->set_flashdata('message', 'Update Record Success');
            redirect(site_url('datamahasiswa'));
        }
    }
    
    public function delete($id) 
    {
        $row = $this->Datamahasiswa_model->get_by_id($id);

        if ($row) {
            $this->Datamahasiswa_model->delete($id);
            $this->session->set_flashdata('message', 'Delete Record Success');
            redirect(site_url('datamahasiswa'));
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('datamahasiswa'));
        }
    }

    public function _rules() 
    {
	$this->form_validation->set_rules('nim', 'nim', 'trim|required');
	$this->form_validation->set_rules('nama', 'nama', 'trim|required');
	$this->form_validation->set_rules('tempatlahir', 'tempatlahir', 'trim|required');
	$this->form_validation->set_rules('tanggallahir', 'tanggallahir', 'trim|required');
	$this->form_validation->set_rules('alamat', 'alamat', 'trim|required');

	$this->form_validation->set_rules('id', 'id', 'trim');
	$this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
    }

    public function excel()
    {
        $this->load->helper('exportexcel');
        $namaFile = "datamahasiswa.xls";
        $judul = "datamahasiswa";
        $tablehead = 0;
        $tablebody = 1;
        $nourut = 1;
        //penulisan header
        header("Pragma: public");
        header("Expires: 0");
        header("Cache-Control: must-revalidate, post-check=0,pre-check=0");
        header("Content-Type: application/force-download");
        header("Content-Type: application/octet-stream");
        header("Content-Type: application/download");
        header("Content-Disposition: attachment;filename=" . $namaFile . "");
        header("Content-Transfer-Encoding: binary ");

        xlsBOF();

        $kolomhead = 0;
        xlsWriteLabel($tablehead, $kolomhead++, "No");
	xlsWriteLabel($tablehead, $kolomhead++, "Nim");
	xlsWriteLabel($tablehead, $kolomhead++, "Nama");
	xlsWriteLabel($tablehead, $kolomhead++, "Tempatlahir");
	xlsWriteLabel($tablehead, $kolomhead++, "Tanggallahir");
	xlsWriteLabel($tablehead, $kolomhead++, "Alamat");

	foreach ($this->Datamahasiswa_model->get_all() as $data) {
            $kolombody = 0;

            //ubah xlsWriteLabel menjadi xlsWriteNumber untuk kolom numeric
            xlsWriteNumber($tablebody, $kolombody++, $nourut);
	    xlsWriteLabel($tablebody, $kolombody++, $data->nim);
	    xlsWriteLabel($tablebody, $kolombody++, $data->nama);
	    xlsWriteLabel($tablebody, $kolombody++, $data->tempatlahir);
	    xlsWriteLabel($tablebody, $kolombody++, $data->tanggallahir);
	    xlsWriteLabel($tablebody, $kolombody++, $data->alamat);

	    $tablebody++;
            $nourut++;
        }

        xlsEOF();
        exit();
    }

    public function word()
    {
        header("Content-type: application/vnd.ms-word");
        header("Content-Disposition: attachment;Filename=datamahasiswa.doc");

        $data = array(
            'datamahasiswa_data' => $this->Datamahasiswa_model->get_all(),
            'start' => 0
        );
        
        $this->load->view('datamahasiswa/datamahasiswa_doc',$data);
    }

}

/* End of file Datamahasiswa.php */
/* Location: ./application/controllers/Datamahasiswa.php */
/* Please DO NOT modify this information : */
/* Generated by Harviacode Codeigniter CRUD Generator 2021-03-08 01:55:34 */
/* http://harviacode.com */