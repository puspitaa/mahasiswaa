<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>Paket Bis</title>
<link href="assetsutama/images/logo-javawebmedia.png" rel="shortcut icon">
<link href="assetsutama/css/style.css" rel="stylesheet">
<!-- Jquery -->
<link href="assetsutama/jquery/jquery-ui.css" rel="stylesheet">
</head>

<body>
<section class="container">
    <header></header>
    <nav>
        <ul>
            <li><a href="<?php echo site_url('datamahasiswa');?>">Mahasiswa</a></li>
        </ul>
    </nav>
    <article>

        <div id="Container">
        <!--  menampilkan data rumah makan, sehingga kita panggil contentnya  -->
            <?php echo $_content ?>
        </div>

    </article>
    <!-- Footer Area -->
    <script src="assetsutama/jquery/external/jquery/jquery.js"></script>
    <script src="assetsutama/jquery/jquery-ui.js"></script>
    <!-- Utk datepicker -->
    <script type="text/javascript">
    $( "#tanggal_lahir" ).datepicker({
        inline: true,
        dateFormat: "yy-mm-dd",
        changeYear: true,
        changeMonth: true,
        numberOfMonths: 1
    });
    </script>
    
    <div class="clearfix"></div>
</section>
</body>
</html>
