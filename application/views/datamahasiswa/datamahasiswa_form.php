<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>Paket Bis</title>
<link href="<?php echo base_url('assetsutama/images/logo-javawebmedia.png" rel="shortcut icon') ?>">
<link href="<?php echo base_url('assetsutama/css/style.css" rel="stylesheet') ?>">
        <link rel="stylesheet" href="<?php echo base_url('assets/bootstrap/css/bootstrap.min.css') ?>"/>
</head>

<body>
<section>
    <article>

        <div>
        <h2 style="margin-top:0px">Input Data Mahasiswa</h2>
        <form action="<?php echo $action; ?>" method="post">
        <div class="form-group">
            <label for="varchar">NIM <?php echo form_error('nim') ?></label>
            <input type="text" class="form-control" name="nim" id="nim" placeholder="Nim" value="<?php echo $nim; ?>" />
        </div>
        <div class="form-group">
            <label for="varchar">Nama <?php echo form_error('nama') ?></label>
            <input type="text" class="form-control" name="nama" id="nama" placeholder="Nama" value="<?php echo $nama; ?>" />
        </div>
        <div class="form-group">
            <label for="tempatlahir">Tempat Lahir <?php echo form_error('tempatlahir') ?></label>
            <textarea class="form-control" rows="3" name="tempatlahir" id="tempatlahir" placeholder="Tempatlahir"><?php echo $tempatlahir; ?></textarea>
        </div>
        <div class="form-group">
            <label for="date">Tanggal Lahir <?php echo form_error('tanggallahir') ?></label>
            <input type="date" class="form-control" name="tanggallahir" id="tanggallahir" placeholder="Tanggallahir" value="<?php echo $tanggallahir; ?>" />
        </div>
        <div class="form-group">
            <label for="alamat">Alamat <?php echo form_error('alamat') ?></label>
            <textarea class="form-control" rows="3" name="alamat" id="alamat" placeholder="Alamat"><?php echo $alamat; ?></textarea>
        </div>
        <input type="hidden" name="id" value="<?php echo $id; ?>" /> 
        <button type="submit" class="btn btn-primary"><?php echo $button ?></button> 
        <a href="<?php echo site_url('datamahasiswa') ?>" class="btn btn-default">Cancel</a>
        </div>

    </article>
    <!-- Footer Area -->
    <script src="<?php echo base_url('assetsutama/jquery/external/jquery/jquery.js') ?>"></script>
    <script src="<?php echo base_url('assetsutama/jquery/jquery-ui.js') ?>"></script>
    <!-- Utk datepicker -->
    
    <div class="clearfix"></div>
</section>
</body>
</html>