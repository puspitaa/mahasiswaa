<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>Paket Bis</title>
<link href="<?php echo base_url('assetsutama/images/logo-javawebmedia.png" rel="shortcut icon') ?>">
<link href="<?php echo base_url('assetsutama/css/style.css" rel="stylesheet') ?>">
        <link rel="stylesheet" href="<?php echo base_url('assets/bootstrap/css/bootstrap.min.css') ?>"/>
</head>

<body>
<section>
    <article>

        <div>
        <h2 style="margin-top:0px">List Data Mahasiswa</h2>
        <div class="row" style="margin-bottom: 10px">
            <div class="col-md-4">
                <?php echo anchor(site_url('datamahasiswa/create'),'Create', 'class="btn btn-primary"'); ?>
            </div>
            <div class="col-md-4 text-center">
                <div style="margin-top: 8px" id="message">
                    <?php echo $this->session->userdata('message') <> '' ? $this->session->userdata('message') : ''; ?>
                </div>
            </div>
            <div class="col-md-1 text-right">
            </div>
            <div class="col-md-3 text-right">
                <form action="<?php echo site_url('datamahasiswa/index'); ?>" class="form-inline" method="get">
                    <div class="input-group">
                        <input type="text" class="form-control" name="q" value="<?php echo $q; ?>">
                        <span class="input-group-btn">
                            <?php 
                                if ($q <> '')
                                {
                                    ?>
                                    <a href="<?php echo site_url('datamahasiswa'); ?>" class="btn btn-default">Reset</a>
                                    <?php
                                }
                            ?>
                          <button class="btn btn-primary" type="submit">Search</button>
                        </span>
                    </div>
                </form>
            </div>
        </div>
        <table class="table table-bordered" style="margin-bottom: 10px">
            <tr>
                <th>No</th>
        <th>Nim</th>
        <th>Nama</th>
        <th>Tempatlahir</th>
        <th>Tanggallahir</th>
        <th>Alamat</th>
        <th>Action</th>
            </tr><?php
            foreach ($datamahasiswa_data as $datamahasiswa)
            {
                ?>
                <tr>
            <td width="80px"><?php echo ++$start ?></td>
            <td><?php echo $datamahasiswa->nim ?></td>
            <td><?php echo $datamahasiswa->nama ?></td>
            <td><?php echo $datamahasiswa->tempatlahir ?></td>
            <td><?php echo $datamahasiswa->tanggallahir ?></td>
            <td><?php echo $datamahasiswa->alamat ?></td>
            <td style="text-align:center" width="200px">
                <?php 
                echo anchor(site_url('datamahasiswa/read/'.$datamahasiswa->id),'Read'); 
                echo ' | '; 
                echo anchor(site_url('datamahasiswa/update/'.$datamahasiswa->id),'Update'); 
                echo ' | '; 
                echo anchor(site_url('datamahasiswa/delete/'.$datamahasiswa->id),'Delete','onclick="javasciprt: return confirm(\'Are You Sure ?\')"'); 
                ?>
            </td>
        </tr>
                <?php
            }
            ?>
        </table>
        <div class="row">
            <div class="col-md-6">
                <a href="#" class="btn btn-primary">Total Record : <?php echo $total_rows ?></a>
        </div>
            <div class="col-md-6 text-right">
                <?php echo $pagination ?>
            </div>
        </div>

    </article>
    <!-- Footer Area -->
    <script src="<?php echo base_url('assetsutama/jquery/external/jquery/jquery.js') ?>"></script>
    <script src="<?php echo base_url('assetsutama/jquery/jquery-ui.js') ?>"></script>
    <!-- Utk datepicker -->
    
    <div class="clearfix"></div>
</section>
</body>
</html>